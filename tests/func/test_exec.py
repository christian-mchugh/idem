# Import Python libs
import subprocess
import os
import asyncio

# Import pop libs
import pop.hub

CODE_DIR = os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))


def run_ex(path, args, kwargs, acct_file=None, acct_key=None):
    """
    Pass in an sls list and run it!
    """
    name = "test"
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    hub.states.test.ACCT = ["test_acct"]
    # This will only work in python 3.7, but idem works in 3.6...
    return asyncio.run(hub.idem.ex.run(path, args, kwargs, acct_file, acct_key))


def test_shell_exec():
    runpy = os.path.join(CODE_DIR, "run.py")
    cmd = f"python3 {runpy} exec test.ping"
    ret = subprocess.run(cmd, stdout=subprocess.PIPE, shell=True)
    assert b"True" in ret.stdout


def test_exec():
    acct_fn = os.path.join(CODE_DIR, "tests", "files", "acct.fernet")
    acct_key = "eWO2UroAYY3Dff8uKcT32iiBHW2qVkVaDV3vIQoIaJU="
    ret = run_ex("test.ctx", [], {}, acct_file=acct_fn, acct_key=acct_key)
    assert ret == {"acct": {"foo": "bar"}}
