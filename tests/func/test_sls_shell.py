# Import Python libs
import subprocess
import os

CODE_DIR = os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))


def test_sls_shell():
    runpy = os.path.join(CODE_DIR, "run.py")
    tree = os.path.join(CODE_DIR, "tests", "sls")
    cmd = f"python3 {runpy} state simple --sls-sources file://{tree}"
    ret = subprocess.run(cmd, stdout=subprocess.PIPE, shell=True)
    assert b"True" in ret.stdout
    assert b"Success!" in ret.stdout
    assert b"Failure!" in ret.stdout
    assert b"happy" in ret.stdout
    assert b"sad" in ret.stdout


def test_sls_implied_source():
    runpy = os.path.join(CODE_DIR, "run.py")
    sls = os.path.join(CODE_DIR, "tests", "sls", "simple.sls")
    cmd = f"python3 {runpy} state {sls}"
    ret = subprocess.run(cmd, stdout=subprocess.PIPE, shell=True)
    assert b"True" in ret.stdout
    assert b"Success!" in ret.stdout
    assert b"Failure!" in ret.stdout
    assert b"happy" in ret.stdout
    assert b"sad" in ret.stdout


def test_sls_tree():
    runpy = os.path.join(CODE_DIR, "run.py")
    tree = os.path.join(CODE_DIR, "tests", "sls")
    cmd = f"python3 {runpy} state simple --tree {tree}"
    ret = subprocess.run(cmd, stdout=subprocess.PIPE, shell=True)
    assert b"True" in ret.stdout
    assert b"Success!" in ret.stdout
    assert b"Failure!" in ret.stdout
    assert b"happy" in ret.stdout
    assert b"sad" in ret.stdout
